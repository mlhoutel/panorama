#!/bin/bash
pkg_dists=( node16-win node16-win-x64 node16-linux node16-linux-x64  )
for dist in "${pkg_dists[@]}"
do
    pkg -o dist/panorama_pkg_$dist.exe -t $dist  index.js 
done
