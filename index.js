#!/usr/bin/node
main();

function main() {
    try {
        const path = require('path');
        const fs = require('fs');

        const { input, output } = parseInputs();
        const directory = decodeURI(path.dirname(input));

        const datas = readFile(decodeURI(input));

        console.info(`source opened at '${input}'`);

        const { JSDOM } = require("jsdom");

        const dom = new JSDOM(datas);
        const document = dom.window.document;

        const svg = document.body.querySelectorAll("svg")?.[0];

        if (svg) {
            console.info(`svg component found in the file`);

            const images = svg.querySelectorAll("image");

            for (const image of [...images]) {

                const xlink = image.getAttributeNS('http://www.w3.org/1999/xlink', 'href')
                const href = image.getAttribute("href")
                const source = decodeURI(xlink || href);
                const relative = path.join(directory, source)

                // Check if the image source is not an svg
                if (!source?.includes(".svg")) {

                    // If relative ressource, copy the file to the output and pass
                    if (fs.existsSync(relative)) {
                        copyFile(relative, path.join(output, source))
                        console.info(` - source is not a svg ('${source}'). Copy to output and skip.`);
                    } else {
                        console.info(` - source is not a svg ('${source}'), skipping.`);
                    }

                    continue;
                }

                const height = image.getAttribute("height");
                const width = image.getAttribute("width");
                const x = image.getAttribute("x");
                const y = image.getAttribute("y");

                // replace the image element with a sub svg element
                let slide;

                try {
                    slide = readFile(relative);
                } catch (e) {
                    // The slide doesnt exist, we don't replace and we go to the next
                    console.info(` - slide not found for '${relative}', skipping.`);
                    continue;
                }

                console.info(` - slide source found for '${source}'...`);

                const fragment = JSDOM.fragment(slide);

                const root = fragment.querySelectorAll("svg")?.[0];

                if (height) root?.setAttribute("height", height);
                if (width) root?.setAttribute("width", width);
                if (x) root?.setAttribute("x", x);
                if (y) root?.setAttribute("y", y);

                image.parentNode.replaceChild(fragment, image);

                console.info(`    <image> succefully replaced by source <svg>`);
            }
        }

        // Write the result in a file with the input name
        const result = path.join(output, path.basename(input));
        createDir(result);

        if (result.includes(".html")) {
            writeFile(result, dom.serialize()); // HTML DOM
        } else {
            writeFile(result, svg.outerHTML); // SVG DOM, no headers
        }


        console.info(`output file written in '${result}'`);
    } catch (err) {
        console.error(err.message);
    }
}

function parseInputs() {
    const { Command } = require('commander');
    const path = require('path');

    // try to find an html file in the dir for the default input
    const files = listFiles(".");
    const html = new RegExp('.*\.html', 'i');
    const match = [...files].find((f) => f.match(html));
    const name = path.basename(match || "default.html");

    // default values
    const SOURCE_SOZI = name;
    const OUTPUT_FILE = `output/`;

    // parser and arguments
    const program = new Command();

    program
        .name('panorama')
        .description('Replace the images in a target svg with sources svgs.')
        .version('0.1.0');

    program
        .option('-i, --input <path>', `.html or.svg file containing the <image> tags`, SOURCE_SOZI)
        .option('-o, --output <path>', `the folder where the result should be stored`, OUTPUT_FILE)
        .parse(process.argv);

    return program.opts();
}


function readFile(location) {
    const fs = require('fs');
    try {
        return fs.readFileSync(location, 'utf8')
    } catch (error) {
        throw new Error(`An error occured while trying to read the ${location} file (${error}).`);
    }
}

function copyFile(source, destination) {
    const fs = require('fs');
    try {
        fs.copyFileSync(source, destination, fs.constants.COPYFILE_FICLONE)
    } catch (error) {
        throw new Error(`An error occured while trying to copy the ${source} file(${error}).`);
    }
}

function listFiles(location) {
    const fs = require('fs');
    try {
        return fs.readdirSync(location, 'utf8');
    } catch (error) {
        throw new Error(`An error occured while trying to read the files in ${location} (${error}).`);
    }
}

function createDir(location) {
    const fs = require('fs');
    const path = require('path');
    const dirname = path.dirname(location);

    try {
        return fs.mkdirSync(dirname, { recursive: true });
    } catch (error) {
        throw new Error(`An error occured while trying to create the ${location} directory(${error}).`);
    }
}

function writeFile(location, data) {
    const fs = require('fs');

    try {
        fs.writeFileSync(location, data);
    } catch (error) {
        throw new Error(`An error occured while trying to write in ${location} (${error}).`);
    }
}
